<?php
session_start();
require 'admin/config.php';
require 'functions.php';

$conexion = conexion($bd_config);
if(!$conexion){
  header("Location: error.php");
}

if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['busqueda'])){
  $busqueda = limpiar_datos($_GET['busqueda']);
  $statement = $conexion->prepare("select * from articulos where titulo like :busqueda or texto like :busqueda");
  $statement->execute(array(":busqueda" => "%$busqueda%"));
  $articulos = $statement->fetchAll();

  if(empty($articulos)){
    $titulo = "No se encontraron articulos con el resultado: " . $busqueda;
  }else{
    $titulo = "Resultados de la busqueda: " . $busqueda;
  }
}else{
  header("Location: " . RUTA . "index.php");
}
require 'views/buscar.view.php';
 ?>
