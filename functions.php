<?php

function conexion($bd_config){
  try {
    $conexion = new PDO("mysql:host=localhost;dbname=".$bd_config['bd'],$bd_config['usuario'],$bd_config['password']);
    return $conexion;
  } catch (PDOException $e) {
    return false;
  }
}

function limpiar_datos($datos){
  $datos = trim($datos);
  $datos = stripslashes($datos);
  $datos = htmlspecialchars($datos);
  return $datos;
}

function obtener_articulos($post_por_pagina, $conexion){
  $inicio = (pagina_actual() > 1) ? pagina_actual() * $post_por_pagina - $post_por_pagina : 0;
  $sentencia = $conexion->prepare("select SQL_CALC_FOUND_ROWS * from articulos order by fecha desc limit $inicio,$post_por_pagina ");
  $sentencia->execute();
  return $sentencia->fetchAll();
}

function numero_paginas($post_por_pagina,$conexion){
  $total = $conexion->prepare("select FOUND_ROWS() as total");
  $total->execute();
  $total = $total->fetch();
  $total = $total['total'];
  $numero_paginas = ceil($total / $post_por_pagina);
  return $numero_paginas;
}

function pagina_actual(){
  return isset($_GET['p']) ? (int)$_GET['p'] : 1;
}

function id_articulo($id){
  return (int)limpiar_datos($id);
}

function obtener_articulo_por_id($conexion, $id_articulo){
  $resultado = $conexion->query("select * from articulos where id=".$id_articulo);
  $resultado = $resultado->fetchAll();
  return ($resultado) ? $resultado : false;
}

function fecha($fecha){
  $timestamp = strtotime($fecha);
  $meses     = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre");
  $dia       = date('d',$timestamp);
  $mes       = date('m',$timestamp)-1;
  $year      = date('Y',$timestamp);
  $fecha     = "$dia de ".$meses[(int)$mes]." del $year";
  return $fecha;
}

function comprobar_sesion(){
  if(!isset($_SESSION['admin'])){
    header("Location: " . RUTA);
  }
}

 ?>
