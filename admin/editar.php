<?php
session_start();
require 'config.php';
require '../functions.php';

comprobar_sesion();

$conexion = conexion($bd_config);
if(!$conexion){
  header("Location: ../error.php");
}

if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET)){
  $id_articulo = $_GET['id'];
  if(empty($id_articulo)){
    header("Location:" . RUTA . 'admin');
  }
  $articulo = obtener_articulo_por_id($conexion,$id_articulo);
  if(!$articulo){
    header("Location:" . RUTA . "admin");
  }
  $articulo = $articulo[0];

}

if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)){
  $titulo   = limpiar_datos($_POST['titulo']);
  $extracto = limpiar_datos($_POST['extracto']);
  $texto    = $_POST['texto'];
  $id       = $_POST['id'];
  if(!empty($_FILES['thumb']['tmp_name'])){
    $thumb  = $_FILES['thumb']['name'];
    $guardar = "../" . $blog_config['carpeta_imagenes'] . $_FILES['thumb']['name'];
    move_uploaded_file($_FILES['thumb']['tmp_name'],$guardar);
  }else{
    $thumb = $_POST['thumb_guardada'];
  }

  $statement = $conexion->prepare(
    "UPDATE articulos
    SET titulo = :titulo,
    extracto = :extracto,
    texto = :texto,
    thumb = :thumb
    WHERE id = :id
    ");

  $statement->execute(array(
    ":titulo" => $titulo,
    ":extracto" => $extracto,
    ":texto" => $texto,
    ":thumb" => $thumb,
    ":id" => $id
  ));

  header("Location:" . RUTA . "admin");

}

require '../views/editar.view.php';
 ?>
