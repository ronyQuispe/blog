<?php
session_start();
require 'admin/config.php';
require 'functions.php';

if($_SERVER['REQUEST_METHOD'] == 'GET' && !isset($_GET['id'])){
  header("Location: index.php");
}

$conexion = conexion($bd_config);
$id_articulo = id_articulo($_GET['id']);

if(empty($id_articulo)){
  header("Location: index.php");
}

$articulo = obtener_articulo_por_id($conexion,$id_articulo);

if(!$articulo){
  header("Location: index.php");
}

$articulo = $articulo[0];
require 'views/single.view.php';
 ?>
