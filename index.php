<?php
session_start();
require 'admin/config.php';
require 'functions.php';

$conexion = conexion($bd_config);
if(!$conexion){
  header("Location: error.php");
}

$articulos = obtener_articulos($blog_config['post_pagina'],$conexion);
if(!$articulos){
  header("Location: error.php");
}

require 'views/index.view.php';
 ?>
