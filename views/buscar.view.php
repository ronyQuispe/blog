<?php require 'header.php'; ?>
    <div class="contenedor">
      <h2><?php echo $titulo; ?></h2>
      <?php foreach ($articulos as $articulo): ?>
        <div class="post">
          <article>
            <h2 class="titulo"><a href="<?php echo RUTA."single.php?id=".$articulo['id']?>"><?php echo $articulo['titulo']; ?></a></h2>
            <p class="fecha"><?php echo $articulo['fecha'] ?></p>
            <div class="thumb">
              <a href="<?php echo RUTA."single.php?id=".$articulo['id']?>">
                <img src="<?php echo RUTA;?>imagenes/<?php echo $articulo['thumb'];?>" alt="">
              </a>
            </div>
            <p class="extracto"><?php echo $articulo['extracto']; ?></p>
            <a href="<?php echo RUTA."single.php?id=".$articulo['id']?>">Continuar Leyendo...</a>
          </article>
        </div>
      <?php endforeach; ?>
      <?php require 'paginacion.php'; ?>
    </div>
    <?php require 'footer.php' ?>
