<?php require 'header.php'; ?>
    <div class="contenedor">
      <div class="post">
        <article>
          <h2 class="titulo"><?php echo $articulo['titulo']; ?></h2>
          <p class="fecha"><?php echo fecha($articulo['fecha']); ?></p>
          <div class="thumb">
            <a href="#"><img src="<?php echo RUTA;?>imagenes/<?php echo $articulo['thumb']?>" alt=""></a>
          </div>
          <p class="extracto">
            <?php echo nl2br($articulo['texto']); ?>
          </p>
        </article>
      </div>
    </div>
    <?php require 'footer.php' ?>
