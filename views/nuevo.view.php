<?php require 'header.php'; ?>
<div class="contenedor">
  <div class="post">
    <article>
      <h2 class="titulo">Nuevo Articulo</h2>
      <form class="formulario" action="<?php $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
        <input type="text" name="titulo" value="" placeholder="Titulo del articulo">
        <input type="text" name="extracto" value="" placeholder="Ingresa una introduccion del articulo">
        <textarea name="texto" placeholder="Texto del Articulo"></textarea>
        <input type="submit" name="guardar_articulo" value="Publicar">
        <input type="file" name="thumb" value="">
      </form>
    </article>
  </div>
</div>
<?php require 'footer.php'; ?>
