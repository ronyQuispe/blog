<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo RUTA;?>/css/estilos.css">
    <title>Pagina Principal</title>
  </head>
  <body>
    <header>
      <div class="contenedor">
        <div class="logo izquierda">
          <p><a href="<?php echo RUTA;?>">Mi primer Blog</a></p>
        </div>
        <div class="derecha">
          <form action="<?php echo RUTA;?>buscar.php" name="busqueda" id="busqueda" class="buscar" method="get">
            <input type="text" name="busqueda" placeholder="Buscar">
            <button type="submit" class="icono fa fa-search"></button>
          </form>
          <nav class="menu">
            <ul>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <?php if(isset($_SESSION['admin'])): ?>
              <li><a href="<?php echo RUTA . "admin"; ?>"><?php echo $_SESSION['admin']; ?></i></a></li>
              <?php else: ?>
              <li><a href="<?php echo RUTA . "login.php"; ?>">Iniciar Sesion</i></a></li>
              <?php endif; ?>
            </ul>
          </nav>
        </div>
      </div>
    </header>
